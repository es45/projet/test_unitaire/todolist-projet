<?php

namespace App\Tests;

use App\Controller\UserController;
use App\Entity\Items;
use App\Entity\User;
use Exception;
use PHPUnit\Framework\TestCase;
use Carbon\Carbon;

class ToDoListTest extends TestCase
{
    private User $user;
    private UserController $userController;
    private Items $item;

    public function __construct()
    {
        parent::__construct();

        $this->user = new User();
        $this->user->setEmail('test@test.com');
        $this->user->setPassword('password');
        $this->user->setFirstname('test');
        $this->user->setLastname('test');
        $this->user->setAge(Carbon::now()->subYears(18));

        $this->userController = new UserController();

        $this->item->setContent("Content");
        $this->item->setName("Name");
        $this->item->setCreationDate(Carbon::now()->subMinutes(60));

    }

    public function testUserIsValid(): void
    {
        $this->assertTrue($this->userController->isValid($this->user));
    }

    public function testUserEmailIsNotValid(): void
    {
        $this->user->setEmail('@test.com');
        $this->expectException(Exception::class);
        $this->userController->isValid($this->user);
    }

    public function testUserPasswordIsNotValid(): void
    {
        $this->user->setPassword('pass');
        $this->expectException(Exception::class);
        $this->userController->isValid($this->user);
    }

    public function testUserFirstnameIsNotValid(): void
    {
        $this->user->setFirstname('');
        $this->expectException(Exception::class);
        $this->userController->isValid($this->user);
    }

    public function testUserLastnameIsNotValid(): void
    {
        $this->user->setLastname('');
        $this->expectException(Exception::class);
        $this->userController->isValid($this->user);
    }

    public function testUserAgeIsNotValid(): void
    {
        $this->user->setAge(Carbon::now()->subYears(12));
        $this->expectException(Exception::class);
        $this->userController->isValid($this->user);
    }

    public function testToDoListIsValidLength(): void
    {

    }

    public function testToDoListIsNotLength(): void
    {

    }

    public function testUniqueItemName(): void
    {

    }

    public function testIsNotUniqueItemName(): void
    {

    }

    public function testContentIsValidLength(): void
    {

    }

    public function testContentIsNotValidLength(): void
    {

    }

    public function testCreationTimeIsValid(): void
    {

    }

    public function testCreationTimeIsNotValid(): void
    {

    }

    public function testEmailSend(): void
    {

    }
}
