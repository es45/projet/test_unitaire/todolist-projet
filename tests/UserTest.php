<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;

class UserTest extends ApiTestCase
{
    public function testUserCreation($body = []): void
    {
        $response = static::createClient()->request('POST', '/users', ['body' => $body ?: [
            'firstname' => 'antoine',
            'lastname' => 'pollet',
            'email' => 'antoine@gmail.com',
            'password' => 'fgeigzneoezfpek'
        ]]);

        $this->assertResponseStatusCodeSame(200);
    }
}
