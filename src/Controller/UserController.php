<?php

namespace App\Controller;

use App\Entity\Items;
use App\Entity\User;
use Carbon\Carbon;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\Constraint\ExceptionMessage;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mime\Address;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

#[asController]
class UserController extends AbstractController
{

    #[Route('/users', name: 'show_users', methods: ['GET'])]
    public function showUsers(): Response
    {
        $serializer = SerializerBuilder::create()->build();
        $users = $this->getDoctrine()->getRepository('App:User')->findAll();
        $data = $serializer->serialize($users, 'json');
        $response = new Response($data);
        $response->headers->set('Content-application', 'application/json');

        return $response;
    }

    #[Route('/users/{id}', name: 'show_user', methods: ['GET'])]
    public function showUser(User $user): Response
    {
        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->serialize($user, 'json');
        $response = new Response($data);
        $response->headers->set('Content-application', 'application/json');

        return $response;
    }

    #[Route('/users', name: 'add_user', methods: ['POST'])]
    public function addUser(Request $request): Response
    {
        $serializer = SerializerBuilder::create()->build();
        $data = $request->getContent();
        $user = $serializer->deserialize($data, User::class, 'json');
        $user->setAge(Carbon::now()->subYears(18));
        if($this->isValid($user)){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return new Response("User created", Response::HTTP_CREATED);
    }

    #[Route('/users/{id}', name: 'remove', methods: ['DELETE'])]
    public function removeAll(User $user): Response
    {
        $this->getDoctrine()->getManager()->remove($user);

        return new Response("User deleted");
    }

    #[Route('/users/{id}/item', name: 'add_item', methods: ['POST'])]
    public function addItem(Request $request, int $id)
    {
        $serializer = SerializerBuilder::create()->build();
        $data = $request->getContent();
        $item = $serializer->deserialize($data, Items::class, 'json');
        try {
            if($this->checkUniqueItemName($id, $item->getName())) {
                $date = date("Y-m-d H:i:s");
                $item->setCreationDate($date);
                try {
                    if($this->checkTimerBetweenItem($id, $date)) {
                        $user = $this->getDoctrine()->getRepository('App:User')->find($id);
                        $user->addTodolist($item);
                        $todolistSize = sizeof($user->getTodolist());
                        if($todolistSize == 8) {
                            $this->sendEmail($user->getEmail());
                        }
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($item);
                        $em->flush();

                        return new Response("Item created", Response::HTTP_CREATED);
                    }
                } catch (\Exception $e) {
                    return new Response($e->getMessage());
                }
            }
        }
        catch (\Exception $e) {
            return new Response($e->getMessage());
        }
    }

    public function checkUniqueItemName(int $id, string $name): bool
    {
        $user = $this->getDoctrine()->getRepository('App:User')->find($id);
        $todolist = $user->getTodolist();
        foreach($todolist as $item) {
            if($item->getName() == $name) {
                throw new \Exception("Item with name $name already exist");
            }
        }
        return true;
    }

    public function checkTimerBetweenItem(int $id, string $creationDate): bool {
        $user = $this->getDoctrine()->getRepository('App:User')->find($id);
        $todolist = $user->getTodolist();
        if(sizeof($todolist) == 0) {
            return true;
        }

        if(sizeof($todolist) == 10) {
            throw new \Exception("Todolist is already at max sized");
        }
        $itemCreationDate = strtotime($creationDate);
        $lastCreationDate = 0;
        foreach ($todolist as $item) {
            $creation_date = $item->getCreationDate();
            $timestamp = strtotime($creation_date);
            if($timestamp > $lastCreationDate) {
                $lastCreationDate = $timestamp;
            }
        }
        if(($itemCreationDate - $lastCreationDate) < 1800) {
            return true;
        }
        throw new \Exception("Last item was created more than 30 min ago");
    }

    public function sendEmail(string $email) {
        $email = (new Email())
            ->to($email)
            ->text('Vous venez d\'ajouter 8 items dans votre todolist');
//      send email here
//      expect sendEmail to have been called once
    }

    public function isValid(User $user): bool
    {
        if (empty($user->getFirstname())) {
            throw new \Exception("User does not have a firstname");
        }
        if (empty($user->getLastname())) {
            throw new \Exception("User does not have a lastname");
        }
        if (strlen($user->getPassword()) < 8 || strlen($user->getPassword()) > 41) {
            throw new \Exception("User password should be between 8 and 40 characters");
        }
        if (!filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("User email invalid");
        }
        if($user->getAge()->diffInYears(Carbon::now()) <= 12) {
            throw new \Exception("User must at least 13");
        }

        return true;
    }

}